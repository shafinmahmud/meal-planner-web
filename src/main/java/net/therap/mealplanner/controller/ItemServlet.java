package net.therap.mealplanner.controller;

import net.therap.mealplanner.entity.Item;
import net.therap.mealplanner.service.ItemService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author shafin
 * @since 11/30/16
 */
@WebServlet(urlPatterns = "/item")
public class ItemServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ItemService service = new ItemService();
        List<Item> itemList = service.getItemList();

        request.setAttribute("itemListBean", itemList);

        request.getRequestDispatcher("/WEB-INF/view/item.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Item item = new Item();
        item.setName(request.getParameter("name"));

        ItemService service = new ItemService();
        service.insertItem(item);

        processRequest(request, response);
    }
}
