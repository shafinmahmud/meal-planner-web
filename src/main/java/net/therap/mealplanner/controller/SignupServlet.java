package net.therap.mealplanner.controller;

import net.therap.mealplanner.dto.SignUpDto;
import net.therap.mealplanner.exception.AlreadyExistsException;
import net.therap.mealplanner.service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author shafin
 * @since 11/25/2016
 */
@WebServlet(urlPatterns = "/signup")
public class SignupServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("/WEB-INF/view/signup.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String sessionAuth = (String) request.getSession().getAttribute("auth");
        if (sessionAuth != null && sessionAuth.equals("ok")) {
            response.sendRedirect(request.getContextPath() + "/");
            return;
        }

        SignUpDto employeeDto = new SignUpDto();
        request.setAttribute("employee", employeeDto);

        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String sessionAuth = (String) request.getSession().getAttribute("auth");
        if (sessionAuth != null && sessionAuth.equals("ok")) {
            response.sendRedirect(request.getContextPath() + "/");
            return;
        }

        SignUpDto dto = new SignUpDto();
        dto.setFirstName(request.getParameter("fisrtname"));
        dto.setLastName(request.getParameter("lastname"));
        dto.setEmail(request.getParameter("email"));
        dto.setDesignation(request.getParameter("designation"));
        dto.setDepartment(request.getParameter("department"));
        dto.setUserName(request.getParameter("username"));
        dto.setPassword(request.getParameter("password"));

        try {
            EmployeeService service = new EmployeeService();
            service.signupEmployee(dto);

        } catch (AlreadyExistsException e) {
            dto.setErrorMessage(e.getMessage());
            request.setAttribute("employee", dto);

            processRequest(request, response);

            return;
        }

        request.getRequestDispatcher("/WEB-INF/view/home.jsp").forward(request, response);
    }
}
