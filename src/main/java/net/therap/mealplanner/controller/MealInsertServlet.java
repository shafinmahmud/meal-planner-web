package net.therap.mealplanner.controller;

import net.therap.mealplanner.dto.MealDto;
import net.therap.mealplanner.service.MealService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shafin
 * @since 11/30/16
 */
@WebServlet(urlPatterns = "/meal/insert")
public class MealInsertServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.sendRedirect(request.getContextPath() + "/");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        MealDto dto = new MealDto();
        dto.setDay(request.getParameter("day"));
        dto.setHour(request.getParameter("hour"));
        dto.setType(request.getParameter("type"));
        dto.setItems(request.getParameter("items"));

        MealService service = new MealService();
        service.insertMeal(dto);

        processRequest(request, response);
    }
}