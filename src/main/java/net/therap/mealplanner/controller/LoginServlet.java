package net.therap.mealplanner.controller;

import net.therap.mealplanner.dto.LoginDto;
import net.therap.mealplanner.exception.NotExistsException;
import net.therap.mealplanner.service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shafin
 * @since 11/25/2016
 */
@WebServlet(urlPatterns = {"/login", "/logout"})
public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("/WEB-INF/view/login.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if(request.getRequestURI().contains("logout")){
            request.getSession().invalidate();
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        String sessionAuth = (String) request.getSession().getAttribute("auth");
        if (sessionAuth != null && sessionAuth.equals("ok")) {
            response.sendRedirect(request.getContextPath() + "/");
            return;
        }

        LoginDto dto = new LoginDto();
        request.setAttribute("loginBean", dto);
        request.getSession().invalidate();

        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String sessionAuth = (String) request.getSession().getAttribute("auth");
        if (sessionAuth != null && sessionAuth.equals("ok")) {
            response.sendRedirect(request.getContextPath() + "/");
            return;
        }

        LoginDto dto = new LoginDto(request.getParameter("username"),
                request.getParameter("password"));

        EmployeeService service = new EmployeeService();

        try {
            boolean isAuthorized = service.authorizeEmployee(dto);

            if (isAuthorized) {
                request.getSession().setAttribute("auth", "ok");
                response.sendRedirect(request.getContextPath() + "/");
                return;
            } else {
                dto.setErrorMessage("Invalid Password!");
            }
        } catch (NotExistsException e) {
            dto.setErrorMessage(e.getMessage());
        }

        request.setAttribute("loginBean", dto);
        processRequest(request, response);
    }
}
