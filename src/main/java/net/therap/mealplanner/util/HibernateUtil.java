package net.therap.mealplanner.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

/**
 * @author shafin
 * @since 11/17/16
 */
public class HibernateUtil {

    private static final String HIBERNATE_CONFIG_XML_FILE = "hibernate.cfg.xml";
    private static final String HIBERNATE_CONFIG_PROPERTY_FILE = "hibernate.properties";

    private static SessionFactory sessionFactory;

    private static SessionFactory buildXmlConfigSessionFactory() {
        try {
            Configuration configuration = new Configuration();
            configuration.configure(HIBERNATE_CONFIG_XML_FILE);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .build();

            return configuration.buildSessionFactory(serviceRegistry);

        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    private static SessionFactory buildPropertyConfigSessionFactory() {
        try {
            Configuration configuration = new Configuration();

            Properties props = FileUtil.getHibernatePropertyValues(HIBERNATE_CONFIG_PROPERTY_FILE);
            configuration.setProperties(props);
            //configuration.addPackage(props.getProperty("hibernate.entity.package"));

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .build();

            return configuration.buildSessionFactory(serviceRegistry);

        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getPropertyConfigSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = buildPropertyConfigSessionFactory();
        }
        return sessionFactory;
    }

    public static SessionFactory getXmlConfigSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = buildXmlConfigSessionFactory();
        }
        return sessionFactory;
    }

    public static void closeSessionFactory() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }
}