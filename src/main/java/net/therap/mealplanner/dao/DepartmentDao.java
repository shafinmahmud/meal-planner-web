package net.therap.mealplanner.dao;

import net.therap.mealplanner.entity.Department;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * @author shafin
 * @since 11/27/16
 */
public class DepartmentDao extends GenericDao<Department> {

    public Department findByName(String name) {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Department.class);
        criteria.add(Restrictions.eq("name", name));
        Department department = (Department) criteria.uniqueResult();

        session.getTransaction().commit();
        return department;
    }
}
