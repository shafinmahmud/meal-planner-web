package net.therap.mealplanner.dao;

import net.therap.mealplanner.entity.Employee;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * @author shafin
 * @since 11/27/16
 */
public class EmployeeDao extends GenericDao<Employee> {

    public boolean doesExist(String email) {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Employee.class);
        criteria.add(Restrictions.eq("email", email));
        Employee employee = (Employee) criteria.uniqueResult();

        session.getTransaction().commit();
        return employee != null;
    }

    public Employee findByUserName(String userName) {
        Session session = getCurrentSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Employee.class);
        criteria.createAlias("loginInfo", "l");
        criteria.add(Restrictions.eq("l.userName", userName));
        Employee employee = (Employee) criteria.uniqueResult();

        session.getTransaction().commit();
        return employee;
    }
}
