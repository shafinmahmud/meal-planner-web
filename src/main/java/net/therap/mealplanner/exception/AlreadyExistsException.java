package net.therap.mealplanner.exception;

/**
 * @author shafin
 * @since 11/28/16
 */
public class AlreadyExistsException extends RuntimeException {

    public AlreadyExistsException() {
    }

    public AlreadyExistsException(String message) {
        super(message);
    }
}
