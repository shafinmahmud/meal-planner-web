package net.therap.mealplanner.exception;

/**
 * @author shafin
 * @since 11/28/16
 */
public class NotExistsException extends RuntimeException {

    public NotExistsException() {
    }

    public NotExistsException(String message) {
        super(message);
    }
}
