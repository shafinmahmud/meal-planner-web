package net.therap.mealplanner.dto;

/**
 * @author shafin
 * @since 11/30/16
 */
public class MealDto {

    private long id;

    private String day;
    private String type;
    private String hour;
    private String items;

    public MealDto() {
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "MealDto{" +
                "day='" + day + '\'' +
                ", type='" + type + '\'' +
                ", hour='" + hour + '\'' +
                ", items='" + items + '\'' +
                '}';
    }
}
