package net.therap.mealplanner.service;

import net.therap.mealplanner.dao.ItemDao;
import net.therap.mealplanner.entity.Item;
import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

/**
 * @author shafin
 * @since 11/14/16
 */
public class ItemService {

    private ItemDao itemDao;

    public ItemService() {
        this.itemDao = new ItemDao();

    }

    public List<Item> getItemList() {
        return itemDao.findAll();
    }

    public void insertItem(Item item) {
        item.setName(WordUtils.capitalizeFully(item.getName().trim()));
        if (itemDao.findByName(item.getName()) == null) {
            itemDao.saveOrUpdate(item);
        }
    }

    public void insertItems(List<Item> items) {
        for (Item item : items) {
            item.setName(WordUtils.capitalizeFully(item.getName().trim()));
            if (itemDao.findByName(item.getName()) == null) {
                itemDao.saveOrUpdate(item);
            }
        }
    }

    public void updateItem(int id, String newName) {
        Item item = itemDao.find(id);
        if (item != null) {
            item.setName(WordUtils.capitalizeFully(newName.trim()));
            itemDao.update(item);
        }
    }

    public void deleteItem(int id) {
        if (!itemDao.isAssociatedWithMeal(id)) {
            itemDao.delete(id);
        } else {
            itemDao.deleteItemRelations(id);
            itemDao.delete(id);
        }
    }

    public static void main(String[] args) {
        ItemService service = new ItemService();
        List<Item> itemList = service.getItemList();
        System.out.println(itemList.size());
    }
}
