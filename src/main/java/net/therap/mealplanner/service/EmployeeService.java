package net.therap.mealplanner.service;

import net.therap.mealplanner.dao.DepartmentDao;
import net.therap.mealplanner.dao.EmployeeDao;
import net.therap.mealplanner.dto.LoginDto;
import net.therap.mealplanner.dto.SignUpDto;
import net.therap.mealplanner.entity.Department;
import net.therap.mealplanner.entity.Employee;
import net.therap.mealplanner.entity.LoginInfo;
import net.therap.mealplanner.exception.AlreadyExistsException;
import net.therap.mealplanner.exception.NotExistsException;

/**
 * @author shafin
 * @since 11/27/16
 */
public class EmployeeService {

    private EmployeeDao employeeDao;
    private DepartmentDao departmentDao;

    public EmployeeService() {
        this.employeeDao = new EmployeeDao();
        this.departmentDao = new DepartmentDao();
    }

    public boolean authorizeEmployee(LoginDto loginDto) throws NotExistsException {
        Employee employee = employeeDao.findByUserName(loginDto.getUserName());

        if (employee == null) {
            throw new NotExistsException("Invalid User Name!");
        }
        return employee.getLoginInfo().getPassword().equals(loginDto.getPassword());
    }

    public void signupEmployee(SignUpDto signUpDto) throws AlreadyExistsException {

        if (employeeDao.doesExist(signUpDto.getEmail())) {
            throw new AlreadyExistsException("Employee Already Exists with this Email!");
        }

        Employee employee = new Employee();
        employee.setName(signUpDto.getFirstName() + " " + signUpDto.getLastName());
        employee.setEmail(signUpDto.getEmail());
        employee.setDesignation(signUpDto.getDesignation());

        Department department = departmentDao.findByName(signUpDto.getDepartment());
        if (department == null) {
            department = new Department();
            department.setName(signUpDto.getDepartment());
            departmentDao.saveOrUpdate(department);
        }

        employee.setDepartment(department);

        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setUserName(signUpDto.getUserName());
        loginInfo.setPassword(signUpDto.getPassword());

        employee.setLoginInfo(loginInfo);
        employeeDao.saveOrUpdate(employee);

    }

    public static void main(String[] args) {
        SignUpDto dto = new SignUpDto();
        dto.setFirstName("Shafin");
        dto.setLastName("Mahmud");
        dto.setEmail("shafin@therapservices.net");
        dto.setDesignation("Associate Software Engineer");
        dto.setDepartment("Software Development");
        dto.setUserName("shafin");
        dto.setPassword("therap123");

        EmployeeService service = new EmployeeService();
        service.signupEmployee(dto);
    }
}
