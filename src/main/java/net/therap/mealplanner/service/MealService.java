package net.therap.mealplanner.service;

import net.therap.mealplanner.dao.ItemDao;
import net.therap.mealplanner.dao.MealDao;
import net.therap.mealplanner.dao.MealTypeDao;
import net.therap.mealplanner.dto.MealDto;
import net.therap.mealplanner.entity.Item;
import net.therap.mealplanner.entity.Meal;
import net.therap.mealplanner.entity.MealType;
import net.therap.mealplanner.util.DateTimeUtil;
import net.therap.mealplanner.util.RegexUtil;
import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author shafin
 * @since 11/13/2016
 */
public class MealService {

    private static final String HOUR_MINUTE_REGEX = "([0-9]{1,2}):([0-9]{1,2})";

    private ItemDao itemDao;
    private MealDao mealDao;
    private MealTypeDao mealTypeDao;

    public MealService() {
        this.itemDao = new ItemDao();
        this.mealDao = new MealDao();
        this.mealTypeDao = new MealTypeDao();
    }

    public List<MealDto> getMealList() {

        List<Meal> meals = mealDao.findAll();

        List<MealDto> mealPlanList = new ArrayList<>();
        for (Meal meal : meals) {

            MealDto mealDto = new MealDto();
            mealDto.setId(meal.getId());
            mealDto.setDay(Meal.resolveDayType(meal.getDay()));
            mealDto.setType(meal.getMealType().getType());
            mealDto.setHour(DateTimeUtil.getHourFromDate(meal.getMealType().getHour()));

            Set<Item> items = mealDao.findItemSetOfMeal(meal.getId());
            mealDto.setItems(Item.listToString(items));

            mealPlanList.add(mealDto);
        }

        return mealPlanList;
    }

    public void deleteMeal(int id) {
        mealDao.delete(id);
    }

    public void insertMeal(MealDto mealDto) {

        Meal.Day day = Meal.resolveDayType(mealDto.getDay());

        int hour = Integer.valueOf(RegexUtil.getAnyMatched(mealDto.getHour(), HOUR_MINUTE_REGEX, 1));
        int minute = Integer.valueOf(RegexUtil.getAnyMatched(mealDto.getHour(), HOUR_MINUTE_REGEX, 2));
        Date hourDate = DateTimeUtil.getDateFromHour(hour, minute);

        MealType mealType = mealTypeDao.findByTypeHour(mealDto.getType(), hourDate);

        if (mealType == null) {

            mealType = new MealType();
            mealType.setHour(hourDate);
            mealType.setType(mealDto.getType());
            mealTypeDao.saveOrUpdate(mealType);
        }

        Meal meal = mealDao.findByDayMealType(day, mealType);

        if (meal == null) {
            meal = new Meal();
            meal.setDay(day);
            meal.setMealType(mealType);
        }

        String[] itemArray = mealDto.getItems().split(",");
        List<Item> itemList = new ArrayList<>();
        for (String name : itemArray) {

            name = WordUtils.capitalizeFully(name.trim());
            Item item = itemDao.findByName(name);

            if (item == null) {
                item = new Item(name);
                itemDao.saveOrUpdate(item);
            }

            itemList.add(item);
        }

        meal.setItemList(itemList);
        mealDao.saveOrUpdate(meal);
    }

    public void updateMeal(MealDto mealDto) {
        Meal.Day day = Meal.resolveDayType(mealDto.getDay());

        int hour = Integer.valueOf(RegexUtil.getAnyMatched(mealDto.getHour(), HOUR_MINUTE_REGEX, 1));
        int minute = Integer.valueOf(RegexUtil.getAnyMatched(mealDto.getHour(), HOUR_MINUTE_REGEX, 2));
        Date hourDate = DateTimeUtil.getDateFromHour(hour, minute);

        Meal meal = mealDao.find(mealDto.getId());

        if (meal == null) {
            meal = new Meal();
        }

        MealType mealType = mealTypeDao.findByTypeHour(mealDto.getType(), hourDate);

        if (mealType == null) {
            mealType = new MealType();
        }

        mealType.setHour(hourDate);
        mealType.setType(mealDto.getType());
        mealTypeDao.saveOrUpdate(mealType);

        meal.setDay(day);
        meal.setMealType(mealType);

        String[] itemArray = mealDto.getItems().split(",");
        List<Item> itemList = new ArrayList<>();
        for (String name : itemArray) {

            Item item = itemDao.findByName(name);
            if (item == null) {
                item = new Item(name);
                itemDao.saveOrUpdate(item);
            }

            itemList.add(item);
        }

        meal.setItemList(itemList);
        mealDao.saveOrUpdate(meal);
    }

    public static void main(String args[]) {
        MealDto dto = new MealDto();
        dto.setDay("Sunday");
        dto.setHour("9:30");
        dto.setType("Breakfast");
        dto.setItems("bread,butter");

        MealService mealService = new MealService();
        //  mealService.insertMeal(dto);

        List<MealDto> mealPlanList = mealService.getMealList();
        System.out.println(mealPlanList.size());

        for (MealDto mealDto : mealPlanList) {
            System.out.println(mealDto.toString());
        }
    }
}
