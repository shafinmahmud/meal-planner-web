<%@ page import="net.therap.mealplanner.dto.MealDto" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: shafin
  Date: 11/24/16
  Time: 5:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MealPlanner :: Home</title>

    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="lib/css/sidenav.css"/>
    <link rel="stylesheet" type="text/css" href="lib/css/table.css"/>
    <link rel="stylesheet" type="text/css" href="lib/css/style.css"/>

    <style type="text/css">
        <%@include file="/lib/bootstrap/css/bootstrap.min.css" %>
    </style>
    <style type="text/css">
        <%@include file="/lib/css/sidenav.css" %>
    </style>
    <style type="text/css">
        <%@include file="/lib/css/table.css" %>
    </style>
    <style type="text/css">
        <%@include file="/lib/css/style.css" %>
    </style>
</head>
</head>
<body>

<%@ include file="frag/navbar-frag.jsp" %>

<br>
<br><br>
<br>
<br>

<%@ include file="frag/sidemenu-frag.jsp" %>

<div class="container">
    <%
        List<MealDto> planList = (ArrayList<MealDto>) request.getAttribute("mealBean");
    %>
    <div>
        <table class="table table-striped custab">
            <thead>
            <a href="#" class="btn btn-primary btn-xs pull-right" data-toggle="modal"
               data-target="#new-plan"><b>+</b> Add New Meal Plan</a>
            <tr>
                <th>Day</th>
                <th>Type</th>
                <th>Time</th>
                <th>Menu</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>

            <%
                for (MealDto plan : planList) {
            %>
            <tr>
                <td rowspan="1"><%=plan.getDay()%>
                </td>
                <td><%=plan.getType()%></td>
                <td><%=plan.getHour()%></td>
                <td><%=plan.getItems()%>
                </td>
                <td class="text-center">
                    <button data-toggle="modal"
                            data-target="#edit-plan-<%=plan.getId()%>"
                            class="btn btn-outline btn-success btn-xs">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </button>

                    <button type="button" data-toggle="modal"
                            data-target="#delete-plan-<%=plan.getId()%>"
                            class="btn btn-outline btn-danger btn-xs">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>
                </td>
            </tr>
            <%
                }
            %>

        </table>
    </div>
</div>


<!-- Modal starts -->
<div id="new-plan" class="modal fade" tabindex="-1"
     aria-labelledby="edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">X
                </button>
                <h4>Insert New Plan</h4>
            </div>
            <div class="modal-body">
                <form action="meal/insert" method="post" class="form-horizontal">
                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="select-day">Day</label>

                        <div class="col-md-4">
                            <select id="select-day" name="day" class="form-control">
                                <option value="0">Choose Day</option>
                                <option value="sunday">Sunday</option>
                                <option value="monday">Monday</option>
                                <option value="tuesday">Tuesday</option>
                                <option value="wednesday">Wednesday</option>
                                <option value="thursday">Thursday</option>
                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Hour</label>

                        <div class="col-md-4">
                            <input name="hour" type="text" placeholder="eg: 09:00"
                                   class="form-control input-md">

                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Meal Type</label>

                        <div class="col-md-4">
                            <select name="type" class="form-control">
                                <option value="0">Choose One</option>
                                <option value="Breakfast">Breakfast</option>
                                <option value="Lunch">Lunch</option>
                                <option value="Dinner">Dinner</option>
                                <option value="Snacks">Snacks</option>
                            </select>
                        </div>
                    </div>

                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="items">Item Set</label>

                        <div class="col-md-4">
                            <textarea class="form-control" id="items" name="items"></textarea>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>

                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <!-- /.modal-header -->
    </div>
    <!-- /.modal-content -->
</div>
</div>
<!--  Modal ends -->

<%
    for (MealDto plan : planList) {
%>
<!-- Modal starts -->
<div id="edit-plan-<%=plan.getId()%>" class="modal fade" tabindex="-1"
     aria-labelledby="edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">X
                </button>
                <h4 class="modal-title"><%=plan.getDay()%>
                </h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" action="meal/update" method="post">

                    <input name="id" type="text" style="display: none" value="<%=plan.getId()%>">
                    <input name="day" type="text" style="display: none" value="<%=plan.getDay()%>">


                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Meal Type</label>

                        <div class="col-md-4">
                            <select name="type" class="form-control">
                                <option value="0" <%=plan.getType().equals("") ? "selected" : ""%>>Choose One</option>
                                <option value="Breakfast" <%=plan.getType().equals("Breakfast") ? "selected" : ""%>>
                                    Breakfast
                                </option>
                                <option value="Lunch" <%=plan.getType().equals("Lunch") ? "selected" : ""%>>Lunch
                                </option>
                                <option value="Dinner" <%=plan.getType().equals("Dinner") ? "selected" : ""%>>Dinner
                                </option>
                                <option value="Snacks" <%=plan.getType().equals("Snacks") ? "selected" : ""%>>Snacks
                                </option>
                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="edit-hour">Hour</label>

                        <div class="col-md-4">
                            <input id="edit-hour" name="hour" type="text" placeholder="meal time eg: 09:00"
                                   value="<%=plan.getHour()%>" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="items">Item Set</label>

                        <div class="col-md-4">
                            <textarea class="form-control" name="items"><%=plan.getItems()%>
                            </textarea>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>

                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer ">
            </div>
        </div>
        <!-- /.modal-header -->
    </div>
    <!-- /.modal-content -->
</div>
</div>


<div id="delete-plan-<%=plan.getId()%>" class="modal fade" tabindex="-1"
     aria-labelledby="edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span>
                    Are you sure you want to delete this Meal Plan?
                </div>

            </div>
            <div class="modal-footer ">
                <form action="meal/delete" method="post">
                    <input name="id" type="text" style="display: none" value="<%=plan.getId()%>">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok-sign"></span> Yes
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> No
                    </button>
                </form>

            </div>
        </div>
    </div>
</div>

</div>

<%
    }
%>

<script src="lib/js/jquery-3.1.1.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.min.js"></script>
<script src="lib/js/sidenav.js"></script>

<script>
    <%@include file="/lib/js/jquery-3.1.1.min.js"%>
</script>
<script>
    <%@include file="/lib/bootstrap/js/bootstrap.min.js"%>
</script>
<script>
    <%@include file="/lib/js/sidenav.js"%>
</script>

</body>
</html>
