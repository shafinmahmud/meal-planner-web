<%@ page import="net.therap.mealplanner.entity.Item" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: shafin
  Date: 11/30/16
  Time: 3:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MealPlanner :: Available Items</title>

    <link rel="stylesheet" type="text/css" href="/lib/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/lib/css/sidenav.css"/>
    <link rel="stylesheet" type="text/css" href="/lib/css/table.css"/>
    <link rel="stylesheet" type="text/css" href="/lib/css/style.css"/>

    <style type="text/css">
        <%@include file="/lib/bootstrap/css/bootstrap.min.css" %>
    </style>
    <style type="text/css">
        <%@include file="/lib/css/sidenav.css" %>
    </style>
    <style type="text/css">
        <%@include file="/lib/css/table.css" %>
    </style>
    <style type="text/css">
        <%@include file="/lib/css/style.css" %>
    </style>
<body>
<%@ include file="frag/navbar-frag.jsp" %>

<br>
<br><br>
<br>
<br>

<%@ include file="frag/sidemenu-frag.jsp" %>

<div class="container">

    <div>
        <%
            List<Item> itemList = (List<Item>) request.getAttribute("itemListBean");
        %>
        <table class="table table-striped custab">
            <thead>
            <a href="#" class="btn btn-success btn-xs pull-right" data-toggle="modal"
               data-target="#new-item"><b>+</b> Add New Item</a>
            <tr>
                <th class="text-center">#</th>
                <th>Items</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>

            <%
                for (Item item : itemList) {
            %>
            <tr>
                <td class="text-center"><%=item.getId()%>
                </td>
                <td><%=item.getName()%>
                </td>

                <td class="text-center">
                    <button data-toggle="modal"
                            data-target="#edit-item-<%=item.getId()%>"
                            class="btn btn-outline btn-success btn-xs">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </button>

                    <button type="button" data-toggle="modal"
                            data-target="#delete-item-<%=item.getId()%>"
                            class="btn btn-outline btn-danger btn-xs">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>
                </td>
            </tr>
            <%
                }
            %>

        </table>
    </div>

</div>

<!-- Modal starts -->
<div id="new-item" class="modal fade" tabindex="-1"
     aria-labelledby="edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">X
                </button>
                <h4>Insert New Item</h4>
            </div>
            <div class="modal-body">
                <form action="item" method="post" class="form-horizontal">

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Item Name</label>

                        <div class="col-md-4">
                            <input name="name" type="text" placeholder="eg: Bread, Banana"
                                   class="form-control input-md">

                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>

                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <!-- /.modal-header -->
    </div>
    <!-- /.modal-content -->
</div>
</div>
<!--  Modal ends -->


<%
    for (Item item : itemList) {
%>
<!-- Modal starts -->
<div id="edit-item-<%=item.getId()%>" class="modal fade" tabindex="-1"
     aria-labelledby="edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">X
                </button>
            </div>

            <div class="modal-body">
                <form action="item/update" method="post" class="form-horizontal">

                    <input name="id" type="text" style="display: none" value="<%=item.getId()%>">

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Item Name</label>

                        <div class="col-md-4">
                            <input name="name" type="text" placeholder="eg: Bread, Banana"
                                   class="form-control input-md" value="<%=item.getName()%>">

                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>

                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer ">
            </div>
        </div>
        <!-- /.modal-header -->
    </div>
    <!-- /.modal-content -->
</div>
<!--  Modal ends -->

<!-- Modal starts -->
<div id="delete-item-<%=item.getId()%>" class="modal fade" tabindex="-1"
     aria-labelledby="edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span>
                    Are you sure you want to delete this Meal Plan?
                </div>

            </div>
            <div class="modal-footer ">
                <form action="item/delete" method="post">
                    <input name="id" type="text" style="display: none" value="<%=item.getId()%>">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok-sign"></span> Yes
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> No
                    </button>
                </form>

            </div>
        </div>
    </div>
</div>
<!--  Modal ends -->
<%
    }
%>


<script src="lib/js/jquery-3.1.1.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.min.js"></script>
<script src="lib/js/sidenav.js"></script>

<script>
    <%@include file="/lib/js/jquery-3.1.1.min.js"%>
</script>
<script>
    <%@include file="/lib/bootstrap/js/bootstrap.min.js"%>
</script>
<script>
    <%@include file="/lib/js/sidenav.js"%>
</script>
</body>
</html>
