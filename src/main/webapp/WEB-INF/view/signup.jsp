<%@ page import="net.therap.mealplanner.dto.SignUpDto" %>
<%--
  Created by IntelliJ IDEA.
  User: SHAFIN
  Date: 11/25/2016
  Time: 6:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MealPlanner :: Signup</title>

    <link rel="stylesheet" href=""/>
    <link rel="stylesheet" type="text/css" ref="lib/bootstrap/css/bootstrap.min.css"/>

    <style type="text/css">
        <%@include file="/lib/bootstrap/css/bootstrap.min.css" %>
    </style>
    <style type="text/css">
        <%@include file="/lib/css/style.css" %>
    </style>
</head>
<body>

<br>
<br>
<br>

<div class="container">

    <%
        SignUpDto signupBean = (SignUpDto) request.getAttribute("employee");
    %>

    <form class="form-horizontal" action="signup" method="post">
        <fieldset>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="firstname">Name</label>

                <div class="col-md-2">
                    <input id="firstname" name="firstname" type="text" placeholder="your first name"
                           class="form-control input-md" required="">
                </div>
                <div class="col-md-2">
                    <input id="lastname" name="lastname" type="text" placeholder="your last name"
                           class="form-control input-md" required="">
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="email">Email</label>

                <div class="col-md-4">
                    <input id="email" name="email" type="text" placeholder="your email" class="form-control input-md"
                           required="">

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="username">User Name</label>

                <div class="col-md-4">
                    <input id="username" name="username" type="text" placeholder="your user id"
                           class="form-control input-md"
                           required="">
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="designation">Designation</label>

                <div class="col-md-4">
                    <input id="designation" name="designation" type="text" placeholder="your designation"
                           class="form-control input-md" required="">

                </div>
            </div>

            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="department">Department</label>

                <div class="col-md-4">
                    <select id="department" name="department" class="form-control">
                        <option value="0" selected>Choose Department</option>
                        <option value="Software Developer">Software Development</option>
                        <option value="Software Quality Assurance">Software Quality Assurance</option>
                        <option value="Database Administration">Database Administration</option>
                        <option value="Operation">Operation</option>
                    </select>
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Password</label>

                <div class="col-md-4">
                    <input id="password" name="password" type="password" placeholder="type password"
                           class="form-control input-md" required="">

                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="re-password">Retype Password</label>

                <div class="col-md-4">
                    <input id="re-password" name="re-password" type="password" placeholder="retype password"
                           class="form-control input-md" required="">

                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="signup-btn"></label>

                <div class="col-md-4">
                    <button id="signup-btn" type="submit" class="btn btn-primary">Submit</button>
                    <%
                        if (signupBean.getErrorMessage() != null) {
                    %>
                    <span><%=signupBean.getErrorMessage()%></span>
                    <%
                        }
                    %>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4"></label>

                <div class="col-md-4">
                    Already have an account? <a href="login" class="text-center new-account">Login Here. </a>
                </div>
            </div>
        </fieldset>
    </form>

</div>
<script src="lib/js/jquery-3.1.1.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.min.js"></script>

<script>
    <%@include file="/lib/js/jquery-3.1.1.min.js"%>
</script>
<script>
    <%@include file="/lib/bootstrap/js/bootstrap.min.js"%>
</script>
</body>
</html>
