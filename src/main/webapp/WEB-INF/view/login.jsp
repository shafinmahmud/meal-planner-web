<%@ page import="net.therap.mealplanner.dto.LoginDto" %>
<%--
  Created by IntelliJ IDEA.
  User: SHAFIN
  Date: 11/25/2016
  Time: 6:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MealPlanner :: Login</title>

    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css"/>

    <style type="text/css">
        <%@include file="/lib/bootstrap/css/bootstrap.min.css" %>
    </style>
    <style type="text/css">
        <%@include file="/lib/css/style.css" %>
    </style>
</head>
<body>

<br>
<br>
<br>
<br>

<div class="container center-div">

    <%
        LoginDto loginBean = (LoginDto) request.getAttribute("loginBean");
    %>

    <form class="form-horizontal" action="login" method="post">
        <%--<h2>Meal Planner :: Login</h2>--%>
        <fieldset>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="username">User Name</label>

                <div class="col-md-3">
                    <input id="username" name="username" type="text" placeholder="your user name"
                           class="form-control input-md" required="">

                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Password</label>

                <div class="col-md-3">
                    <input id="password" name="password" type="password" placeholder="your password"
                           class="form-control input-md" required="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label"></label>

                <div class="checkbox col-md-3">
                    <label>
                        <input type="checkbox"> Remember me
                    </label>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for=""></label>

                <div class="col-md-4">
                    <button id="" name="" class="btn btn-primary">Login</button>
                    <%
                        if (loginBean.getErrorMessage() != null) {
                    %>
                    <span><%=loginBean.getErrorMessage()%></span>
                    <%
                        }
                    %>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label"></label>

                <div class="col-md-3">
                    New User?<a href="signup" class="text-center new-account"> Sign Up Here</a>
                </div>
            </div>

        </fieldset>
    </form>

</div>
<script>
    <%@include file="/lib/js/jquery-3.1.1.min.js"%>
</script>
<script>
    <%@include file="/lib/bootstrap/js/bootstrap.min.js"%>
</script>
</body>
</html>
